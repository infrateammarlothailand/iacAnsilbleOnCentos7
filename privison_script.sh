#!/bin/bash
sudo rpm -iUvh https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
sudo yum -y update
sudo yum -y install ansible
ansible --version
if [ $? == 0 ]; then
	echo "Successfully install Ansible"
else
	echo "Failed to install Ansible"
fi

sudo yum -y install vim
sudo yum -y install git
sudo yum -y install mlocate
sudo updatedb

